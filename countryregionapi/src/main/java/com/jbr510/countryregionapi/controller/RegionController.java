package com.jbr510.countryregionapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jbr510.countryregionapi.model.Region;
import com.jbr510.countryregionapi.service.*;
@CrossOrigin
@RestController
public class RegionController {
    @GetMapping("/region-info")
    public Region getRegionByRegionCode(
        @RequestParam(required = true, name = "regionCode") String paramRegionCode
    ) {
        int i = 0;
        boolean isFounded = false;
        Region result = null;
        while (isFounded == false && i < RegionService.getAllRegionLists().size()) {
            if (RegionService.getAllRegionLists().get(i).getRegionCode().equals(paramRegionCode)) {
                isFounded = true;
                result = RegionService.getAllRegionLists().get(i);
            }
            else {
                i ++;
            }
        }
        return result;
    }
}