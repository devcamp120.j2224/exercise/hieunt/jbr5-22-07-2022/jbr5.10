package com.jbr510.countryregionapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jbr510.countryregionapi.model.*;
import com.jbr510.countryregionapi.service.*;

@CrossOrigin
@RestController
public class CountryController {
    // @Autowired
    // static CountryService countries;

    @GetMapping("/countries")
    public ArrayList<Country> getCountryList() throws Exception {
        ArrayList<Country> allCountries = CountryService.getCountryList();
        return allCountries;
    }

    @GetMapping("/country-info")
    public Country getCountryByCountryCode(
        @RequestParam(required = true, name = "countryCode") String paramCountryCode
    ) {
        int i = 0;
        boolean isFounded = false;
        Country result = null;
        ArrayList<Country> countryList = CountryService.getCountryList();
        while (isFounded == false && i < countryList.size()) {
            Country bCountry = countryList.get(i);
            if(bCountry.getCountryCode().equals(paramCountryCode)) {
                isFounded = true;
                result = bCountry;
            }
            else {
                i ++;
            }
        }
        return result;
    }

    @GetMapping("/country")
    public Map<String, Object> getRegionListOfCountry(
        @RequestParam(required = true, name = "countryCode") String paramCountryCode)
    throws Exception {
        Map<String, Object> responseObj = new HashMap<String, Object>();
        ArrayList<Region> foundedRegionList = null;

        int i = 0;
        boolean isFounded = false;
        ArrayList<Country> countryList = CountryService.getCountryList();
        while (isFounded == false && i < countryList.size()) {
            Country bCountry = countryList.get(i);
            if(bCountry.getCountryCode().equals(paramCountryCode)) {
                isFounded = true;
                foundedRegionList = bCountry.getRegions();
                responseObj.put("regions", foundedRegionList);
                responseObj.put("status", "founded!");
            }
            else {
                i ++;
            }
        }
        if(isFounded == false) { // nếu hết vòng lặp vẫn không tìm thấy
            responseObj.put("regions", null);
            responseObj.put("status", "not founded!");
        }
        return responseObj;
    }

    @PostMapping("/countries/addnew")
    public Country createNewCountry(@RequestBody Country newCountry) {
        CountryService.getCountryList().add(newCountry);
        System.out.println(CountryService.getCountryList());
        return newCountry;
    }
}
