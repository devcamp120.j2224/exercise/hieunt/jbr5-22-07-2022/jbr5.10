package com.jbr510.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jbr510.countryregionapi.model.Country;

@Service
public class CountryService {
    private static ArrayList<Country> countryList = new ArrayList<Country>();
    // @Autowired
    // static RegionService regions;
    static {
        Country countryVN = new Country("VN", "Viet Nam", null);
        Country countryUSA = new Country("USA", "America", null);
        Country countryAUS = new Country("AUS", "Australia", null);
        countryList.add(countryVN);
        countryList.add(countryUSA);
        countryList.add(countryAUS);
        for (int i = 0; i < countryList.size(); i++) {
            if(countryList.get(i).getCountryCode() == "VN") {
                countryList.get(i).setRegions(RegionService.getRegionListVN());
            }
            else if(countryList.get(i).getCountryCode() == "USA") {
                countryList.get(i).setRegions(RegionService.getRegionListUSA());
            }
            else if(countryList.get(i).getCountryCode() == "AUS") {
                countryList.get(i).setRegions(RegionService.getRegionListAUS());
            }
        }
    }
    // getter setter
    public static ArrayList<Country> getCountryList() {
        return countryList;
    }
    public static void setCountryList(ArrayList<Country> countryList) {
        CountryService.countryList = countryList;
    }
    // khởi tạo không tham số
    // public CountryService() {
    //     super();
    // }  
}
