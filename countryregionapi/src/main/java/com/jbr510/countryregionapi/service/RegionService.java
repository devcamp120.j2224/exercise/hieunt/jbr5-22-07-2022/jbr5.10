package com.jbr510.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr510.countryregionapi.model.Region;

@Service
public class RegionService {
    private static ArrayList<Region> regionListVN = new ArrayList<>();
    private static ArrayList<Region> regionListUSA = new ArrayList<>();
    private static ArrayList<Region> regionListAUS = new ArrayList<>();
    private static ArrayList<Region> allRegionLists = new ArrayList<>();
    static {
        Region regionVN1 = new Region("VN1", "Ha Noi");
        Region regionVN2 = new Region("VN2", "Da Nang");
        Region regionVN3 = new Region("VN3", "Ho Chi Minh");
        Region regionUSA1 = new Region("USA1", "Washington");
        Region regionUSA2 = new Region("USA2", "New York");
        Region regionUSA3 = new Region("USA3", "California");
        Region regionAUS1 = new Region("AUS1", "Canberra");
        Region regionAUS2 = new Region("AUS2", "Sydney");
        Region regionAUS3 = new Region("AUS3", "Melbourne");
        regionListVN.add(regionVN1);
        regionListVN.add(regionVN2);
        regionListVN.add(regionVN3);
        regionListUSA.add(regionUSA1);
        regionListUSA.add(regionUSA2);
        regionListUSA.add(regionUSA3);
        regionListAUS.add(regionAUS1);
        regionListAUS.add(regionAUS2);
        regionListAUS.add(regionAUS3);   
        for (Region bRegion : regionListVN) {
            allRegionLists.add(bRegion);
        }
        for (Region bRegion : regionListUSA) {
            allRegionLists.add(bRegion);
        }
        for (Region bRegion : regionListAUS) {
            allRegionLists.add(bRegion);
        }
    }
    // các getter setter
    public RegionService() {
    }
    public static ArrayList<Region> getRegionListVN() {
        return regionListVN;
    }
    public static void setRegionListVN(ArrayList<Region> regionListVN) {
        RegionService.regionListVN = regionListVN;
    }
    public static ArrayList<Region> getRegionListUSA() {
        return regionListUSA;
    }
    public static void setRegionListUSA(ArrayList<Region> regionListUSA) {
        RegionService.regionListUSA = regionListUSA;
    }
    public static ArrayList<Region> getRegionListAUS() {
        return regionListAUS;
    }
    public static void setRegionListAUS(ArrayList<Region> regionListAUS) {
        RegionService.regionListAUS = regionListAUS;
    }
    public static ArrayList<Region> getAllRegionLists() {
        return allRegionLists;
    }
}
